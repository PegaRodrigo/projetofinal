package pucrs.myflight.gui;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingNode;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.jxmapviewer.JXMapKit;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.GeoPosition;

import pucrs.myflight.gui.autocomplete.combobox.ComboBoxAutoComplete;
import pucrs.myflight.modelo.Aeroporto;
import pucrs.myflight.modelo.Rota;
import pucrs.myflight.modelo.gerenciadores.*;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelListener;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.SwingUtilities;

public class JanelaFX extends Application {

	final SwingNode mapkit = new SwingNode();

	private GerenciadorCias gerCias;
	private GerenciadorAeroportos gerAero;
	private GerenciadorRotas gerRotas;
	private GerenciadorAeronaves gerAeronaves;
	private GerenciadorPaises gerPaises;
	private GerenciadorMapa gerenciador;
	private Aeroporto aeroportoSelecionado;

	private EventosMouse mouse;
	private EventosMouse mouse2;

	@Override
	public void start(Stage primaryStage) throws Exception {

		setup();

		GeoPosition poa = new GeoPosition(-30.05, -51.18);
		gerenciador = new GerenciadorMapa(poa, GerenciadorMapa.FonteImagens.VirtualEarth);
		mouse = new EventosMouse();

		JXMapKit mapKit = gerenciador.getMapKit();
		mapKit.setZoom(100);
		mapKit.getMainMap().addMouseListener(mouse);
		mapKit.getMainMap().addMouseMotionListener(mouse);
		
		List<String> aeroportos = gerAero.listagemConsulta3();
		createSwingContent(mapkit);

		BorderPane pane = new BorderPane();
		GridPane leftPane = new GridPane();
		leftPane.setAlignment(Pos.CENTER_LEFT);
		leftPane.setHgap(10);
		leftPane.setVgap(10);
		leftPane.setPadding(new Insets(10, 10, 10, 10));

		ComboBox<String> cbConsulta1 = new ComboBox<String>(FXCollections.observableArrayList(gerCias.listarNomes()));
		cbConsulta1.setPromptText("Companhia A�rea");

		ComboBox<String> cbConsulta2 = new ComboBox<String>(FXCollections.observableArrayList(gerPaises.listarNomes()));
		cbConsulta2.setPromptText("Pa�s");

		ToggleGroup group = new ToggleGroup();
		RadioButton rb1 = new RadioButton("Exibir na escala mundial");
		rb1.setToggleGroup(group);
		rb1.setSelected(true);
		RadioButton rb2 = new RadioButton("Exibir na escala nacional");
		rb2.setToggleGroup(group);

		ComboBox<String> cbConsulta3Origem = new ComboBox<String>(FXCollections.observableArrayList(new ArrayList<String>(aeroportos)));
		cbConsulta3Origem.setPromptText("Selecione ou digite a Origem");
		ComboBox<String> cbConsulta3Destino = new ComboBox<String>(FXCollections.observableArrayList(new ArrayList<String>(aeroportos)));
		cbConsulta3Destino.setPromptText("Selecione ou digite o Destino");

		//Consulta 4:
		List<Integer> horas = new ArrayList<Integer>();
		for(int i = 1; i<=17; i++){
			horas.add(i);
		}
		
		ComboBox<Integer> cbHoras = new ComboBox<Integer>(FXCollections.observableArrayList(horas));
		cbHoras.setPromptText("M�ximo horas de voo");
		ToggleGroup groupConexoes = new ToggleGroup();
		RadioButton rbConexoes = new RadioButton("Com conex�es");
		rbConexoes.setToggleGroup(groupConexoes);
		RadioButton rbSemConexoes = new RadioButton("Sem conex�es");
		rbSemConexoes.setToggleGroup(groupConexoes);
		rbSemConexoes.setSelected(true);


		leftPane.add(new Label("Consulta 1"), 1, 0);
		leftPane.add(cbConsulta1, 1, 3);

		leftPane.add(new Label("Consulta 2"), 3, 0);
		leftPane.add(cbConsulta2, 3, 3);
		leftPane.add(rb1, 3, 1);
		leftPane.add(rb2, 3, 2);

		leftPane.add(new Label("Consulta 3"), 5, 0);
		leftPane.add(cbConsulta3Origem, 5, 2);
		leftPane.add(cbConsulta3Destino, 5, 3);

		leftPane.add(new Label("Consulta 4"), 6, 0);
		leftPane.add(cbHoras, 6, 1);
		leftPane.add(rbConexoes, 6, 2);
		leftPane.add(rbSemConexoes, 6, 3);


		cbConsulta1.setOnAction(e -> {
			if(cbConsulta1.getSelectionModel().getSelectedItem() != null){
				String cia = cbConsulta1.getSelectionModel().getSelectedItem().toString();
				consulta1(cia);
			}
		});

		cbConsulta2.setOnAction(e -> {
			if(cbConsulta2.getSelectionModel().getSelectedItem() != null){
				String codPais = cbConsulta2.getSelectionModel().getSelectedItem().toString();
				if(codPais != "Todos"){
					codPais = codPais.split(" - ")[1];
				}
				String escala;
				if(group.getSelectedToggle().equals(rb1)){escala = "mundial";}
				else{escala = "nacional";}
				consulta2(codPais, escala);
			}
		});

		rb1.setOnAction(e -> {
			if(cbConsulta2.getSelectionModel().getSelectedItem() != null){
				String codPais = cbConsulta2.getSelectionModel().getSelectedItem().toString();
				if(codPais != "Todos"){
					codPais = codPais.split(" - ")[1];
				}
				String escala;
				if(group.getSelectedToggle().equals(rb1)){escala = "mundial";}
				else{escala = "nacional";}
				consulta2(codPais, escala);
			}
		});

		rb2.setOnAction(e -> {
			if(cbConsulta2.getSelectionModel().getSelectedItem() != null){
				String codPais = cbConsulta2.getSelectionModel().getSelectedItem().toString();
				if(codPais != "Todos"){
					codPais = codPais.split(" - ")[1];
				}
				String escala;
				if(group.getSelectedToggle().equals(rb1)){escala = "mundial";}
				else{escala = "nacional";}
				consulta2(codPais, escala);
			}
		});

		cbConsulta3Origem.setOnAction(e -> {
			runConsulta3(cbConsulta3Origem, cbConsulta3Destino);
		});

		cbConsulta3Destino.setOnAction(e2 -> {
			runConsulta3(cbConsulta3Origem, cbConsulta3Destino);
		});

		rbConexoes.setOnAction(e ->{
			if(aeroportoSelecionado != null && cbHoras.getSelectionModel().getSelectedItem() != null){
				boolean conexao;
				if(groupConexoes.getSelectedToggle().equals(rbConexoes)){
					conexao = true;
				}else{
					conexao = false;
				}
				consulta4(cbHoras.getSelectionModel().getSelectedItem().toString(), conexao);
			}
		});
		rbSemConexoes.setOnAction(e ->{
			if(aeroportoSelecionado != null && cbHoras.getSelectionModel().getSelectedItem() != null){
				boolean conexao;
				if(groupConexoes.getSelectedToggle().equals(rbConexoes)){
					conexao = true;
				}else{
					conexao = false;
				}
				consulta4(cbHoras.getSelectionModel().getSelectedItem().toString(), conexao);
			}
		});


		cbHoras.setOnAction(e -> {
			if(aeroportoSelecionado != null && cbHoras.getSelectionModel().getSelectedItem() != null){
				boolean conexao;
				if(groupConexoes.getSelectedToggle().equals(rbConexoes)){conexao = true;}
				else{conexao = false;}
				consulta4(cbHoras.getSelectionModel().getSelectedItem().toString(), conexao);
			}
		});


		new ComboBoxAutoComplete<String>(cbConsulta1);
		new ComboBoxAutoComplete<String>(cbConsulta2);
		new ComboBoxAutoComplete<String>(cbConsulta3Destino);
		new ComboBoxAutoComplete<String>(cbConsulta3Origem);
		new ComboBoxAutoComplete<Integer>(cbHoras);

		pane.setCenter(mapkit);
		pane.setTop(leftPane);

		Scene scene = new Scene(pane, 1600, 700);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Mapas com JavaFX");
		primaryStage.show();

	}

	public void carregaDados() throws IOException {
		gerCias.carregaDados();
		gerAeronaves.carregaDados();
		gerAero.carregaDados();
		gerRotas.carregaDados();
		gerPaises.carregaDados();
	}

	public static void main(String[] args) {
		launch(args);
	}

	private void runConsulta3(ComboBox<String> cbConsulta3Origem, ComboBox<String> cbConsulta3Destino) {
		try {
			String aeroOrigem = cbConsulta3Origem.getSelectionModel().getSelectedItem().toString();
			String aeroDestino = cbConsulta3Destino.getSelectionModel().getSelectedItem().toString();
			aeroOrigem = aeroOrigem.split("-")[1].trim();
			aeroDestino = aeroDestino.split("-")[1].trim();
			consulta3(aeroOrigem, aeroDestino);
		} catch (NullPointerException e) {
		}
	}

	// Inicializando os dados aqui...
	private void setup() {

		gerCias = new GerenciadorCias();
		gerAero = new GerenciadorAeroportos();
		gerAeronaves = new GerenciadorAeronaves();
		gerPaises = new GerenciadorPaises();
		gerRotas = new GerenciadorRotas(gerCias, gerAero, gerAeronaves);
		aeroportoSelecionado = null;

		try {
			carregaDados();
		} catch (IOException e) {
			System.out.println("Erro na leitura dos dados!");
			System.out.println("Msg: " + e);
			System.exit(1);
		}

	}

	private void consulta1(String cia) {
		gerenciador.clear();
		// Lista para armazenar o resultado da consulta
		List<MyWaypoint> lstPoints = new ArrayList<>();
		Aeroporto origem = null;
		Aeroporto destino = null;

		for (Rota rota : gerRotas.getRotasList()) {
			if (rota.getCia().getNome().trim().equals(cia.trim())) {
				origem = rota.getOrigem();
				destino = rota.getDestino();
				lstPoints.add(new MyWaypoint(Color.blue, origem.getNome(), origem.getLocal(), 5));
				lstPoints.add(new MyWaypoint(Color.blue, destino.getNome(), destino.getLocal(), 5));
				Tracado way = new Tracado();
				way.addPonto(origem.getLocal());
				way.addPonto(destino.getLocal());
				way.setWidth(1);
				way.setCor(Color.BLACK);
				gerenciador.addTracado(way);
			}
		}

		JXMapKit mapKit = gerenciador.getMapKit();

		if (Objects.nonNull(origem)) {
			mapKit.setAddressLocation(origem.getLocal());
		}

		gerenciador.setPontos(lstPoints);
		mapKit.repaint();
	}


	public void consulta2(String codPais, String escala) {
		List<MyWaypoint> lstPoints = new ArrayList<>();
		gerenciador.clear();
		final int minSize = 1;
		final int maxSize = 15;
		int rotasAeroportoMaisMovimentado;
		Aeroporto aeroportoFoco = null;

		if(escala.equals("mundial")){rotasAeroportoMaisMovimentado = gerAero.rotasAeroportoMaisMovimentado();}
		else{rotasAeroportoMaisMovimentado = gerAero.rotasAeroportoMaisMovimentado(codPais);}

		final int step = rotasAeroportoMaisMovimentado / maxSize;

		for(Aeroporto aero: gerAero.listarTodos()){
			if(aero.getCodigoPais().equals(codPais) || codPais.equals("Todos")){
				aeroportoFoco = aero;
				Color color;
				int circleSize=0;
				int rotas = aero.getRotasRelacionadas();
				if(rotas < step){circleSize = 1; color = new Color(0,0,255);}
				else if(rotas < step*2){circleSize = 5; color = new Color(0,139,139);}
				else if(rotas < step*3){circleSize = 8; color = new Color(50,205,50);}
				else if(rotas < step*4){circleSize = 11; color = new Color(173,255,47);}
				else if(rotas < step*5){circleSize = 14; color = new Color(255,255,0);}
				else if(rotas < step*6){circleSize = 17; color = new Color(255,215,0);}
				else if(rotas < step*7){circleSize = 20; color = new Color(218,165,32);}
				else if(rotas < step*8){circleSize = 23; color = new Color(255,165,0);}
				else if(rotas < step*9){circleSize = 26; color = new Color(255,140,0);}
				else if(rotas < step*10){circleSize = 29; color = new Color(178,34,34);}
				else if(rotas < step*11){circleSize = 32; color = new Color(128,0,0);}
				else if(rotas < step*12){circleSize = 35; color = new Color(75,0,130);}
				else if(rotas < step*13){circleSize = 38; color = new Color(25,25,112);}
				else if(rotas < step*14){circleSize = 41; color = new Color(105,105,105);}
				else {circleSize = 44; color = new Color(0,0,0);}

				lstPoints.add(new MyWaypoint(color, aero.getNome(), aero.getLocal(), circleSize));
			}
		}
		if (Objects.nonNull(aeroportoFoco) && !codPais.equals("Todos")) {
			gerenciador.getMapKit().setAddressLocation(aeroportoFoco.getLocal());
		}
		gerenciador.setPontos(lstPoints);
		gerenciador.getMapKit().repaint();
	}

	private void consulta3(String codigoOrigem, String codigoDestino) {
		gerenciador.clear();
		List<MyWaypoint> lstPoints = new ArrayList<>();
		String key = codigoOrigem.concat(codigoDestino);
		Map<String, Rota> rotaMap = gerRotas.getRotasMaps();
		Rota mainRoute = rotaMap.get(key);
		JXMapKit mapKit = gerenciador.getMapKit();

		showRotaConexoesFound(codigoOrigem, codigoDestino, lstPoints, rotaMap);
		gerenciador.setPontos(lstPoints);
		if (Objects.nonNull(mainRoute)) {
			mapKit.setAddressLocation(mainRoute.getOrigem().getLocal());
		}
		mapKit.repaint();
	}

	private void showRotaConexoesFound(String codigoOrigem, String codigoDestino, List<MyWaypoint> lstPoints, Map<String, Rota> rotaMap) {
		String key = codigoOrigem.concat(codigoDestino);

		//Set for don't repeat the route keys.
		Set<String> routeKeys = new HashSet<>();
		Set<Rota> rotas = gerRotas.getRotasMaps().values().parallelStream().collect(Collectors.toSet());
		Set<Rota> rotasLigadasAoDestino = new HashSet<>();
		if (rotaMap.containsKey(key) && !routeKeys.contains(key)) {
			Rota mainRoute = rotaMap.get(key);
			getTracadosAndPoints(mainRoute, lstPoints);
		}

		if (!codigoOrigem.equals(codigoDestino)) {
			for (Rota rota : rotas) {
				String keyLigadaAoDestino = rota.getDestino().getCodigo().concat(codigoDestino);
				if (rotaMap.containsKey(keyLigadaAoDestino)) {
					rotasLigadasAoDestino.add(rota);
				}

				//Check if is second way for destiny route.
				key = codigoOrigem.concat(rota.getOrigem().getCodigo());
				if (rotaMap.containsKey(key)) {
					Rota rotaEncontrada = rotaMap.get(key);
					key = rotaEncontrada.getDestino().getCodigo().concat(codigoDestino);
					//Add the second way for destiny route.
					if (rotaMap.containsKey(key)) {
						Rota rotaDoisEncontrada = rotaMap.get(key);
						getTracadosAndPoints(rotaEncontrada, lstPoints);
						getTracadosAndPoints(rotaDoisEncontrada, lstPoints);
					}

					//
					//                    key = rotaEncontrada.getDestino().getCodigo().concat(rota.getOrigem().getCodigo());
					//                    //Add the third way for destiny route.
					//                    if (rotaMap.containsKey(key)) {
					//                        Rota rotaTresEncontrada = rotaMap.get(key);
					//                        key = rotaTresEncontrada.getDestino().getCodigo().concat(codigoDestino);
					//                        if (rotaMap.containsKey(key)) {
					//                            Rota rotaQuatroEncontrada = rotaMap.get(key);
					//                            getTracadosAndPoints(rotaTresEncontrada, lstPoints);
					//                            getTracadosAndPoints(rotaQuatroEncontrada, lstPoints);
					//                        }
					//                    }
				}
			}
		}
	}

	public void consulta4(String horas, boolean conexao) {
		gerenciador.clear();
		Integer tempo = Integer.parseInt(horas);
		List<MyWaypoint> lstPoints = new ArrayList<>();
		Set<Rota> rotas = gerRotas.getRotasMaps().values().parallelStream().collect(Collectors.toSet());

		for(Rota rota: rotas){
			if(rota.getOrigem().equals(aeroportoSelecionado) && (rota.getDuracao().toHours()+1) <= tempo){
				Aeroporto origem = rota.getOrigem();
				Aeroporto destino = rota.getDestino();
				lstPoints.add(new MyWaypoint(Color.blue, origem.getNome(), origem.getLocal(), 5));
				lstPoints.add(new MyWaypoint(Color.blue, destino.getNome(), destino.getLocal(), 5));
				Tracado way = new Tracado();
				way.addPonto(origem.getLocal());
				way.addPonto(destino.getLocal());
				way.setWidth(1);
				way.setCor(Color.BLACK);
				gerenciador.addTracado(way);
			}
		}

		if(conexao == true) {
			for(Rota rota: rotas) {
				if(rota.getOrigem().equals(aeroportoSelecionado)){
					for(Rota rota2: rotas) {
						if(rota.getDestino().equals(rota2.getOrigem())) {
							if(rota.getDuracao().toHours() + rota2.getDuracao().toHours() + 1 <=tempo){
								getTracadosAndPoints(rota, lstPoints);
								getTracadosAndPoints(rota2, lstPoints);
							}
						}
					}
				}
			}
		}

		if (Objects.nonNull(aeroportoSelecionado)) {
			gerenciador.getMapKit().setAddressLocation(aeroportoSelecionado.getLocal());
		}
		gerenciador.setPontos(lstPoints);
		gerenciador.getMapKit().repaint();

	}

	private void getTracadosAndPoints(Rota rota, List<MyWaypoint> lstPoints) {
		lstPoints.add(new MyWaypoint(randNewCor(), rota.getOrigem().getNome(), rota.getOrigem().getLocal(), 8));
		lstPoints.add(new MyWaypoint(randNewCor(), rota.getDestino().getNome(), rota.getDestino().getLocal(), 8));
		Tracado way = new Tracado();
		way.addPonto(rota.getOrigem().getLocal());
		way.addPonto(rota.getDestino().getLocal());
		way.setWidth(2);
		way.setCor(randNewCor());
		gerenciador.addTracado(way);
	}

	private Color randNewCor() {
		Random random = new Random();
		int r = random.nextInt(255);
		int g = random.nextInt(255);
		int b = random.nextInt(255);

		return new Color(r, g, b);
	}

	private class EventosMouse extends MouseAdapter {
		private int lastButton = -1;

		@Override
		public void mousePressed(MouseEvent e) {
			JXMapViewer mapa = gerenciador.getMapKit().getMainMap();
			GeoPosition loc = mapa.convertPointToGeoPosition(e.getPoint());
			lastButton = e.getButton();
			// Bot�o 3: seleciona localiza��o
			if (lastButton == MouseEvent.BUTTON3) {
				gerenciador.setPosicao(loc);
				gerenciador.getMapKit().repaint();
			}
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			JXMapViewer map = gerenciador.getMapKit().getMainMap();
			GeoPosition geoPosition = map.convertPointToGeoPosition(e.getPoint());
			List<MyWaypoint> myWaypoints = new ArrayList<>();
			Double latitudeIndex = geoPosition.getLatitude();
			Double longitudeIndex = geoPosition.getLongitude();

			aeroportoSelecionado = gerAero.buscarGeoIndex(latitudeIndex, longitudeIndex);
			gerenciador.clear();
			try {
				myWaypoints.add(new MyWaypoint(Color.RED, aeroportoSelecionado.getNome(), aeroportoSelecionado.getLocal(), 8));
			} catch (NullPointerException ex) {

			}
			gerenciador.setPontos(myWaypoints);
			gerenciador.getMapKit().repaint();
		}
		
		@Override
		public void mouseDragged(MouseEvent e) {
			JXMapViewer miniMap = gerenciador.getMapKit().getMiniMap();
			gerenciador.getMapKit().getZoomSlider().addNotify();
		}
	}

	private void createSwingContent(final SwingNode swingNode) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				swingNode.setContent(gerenciador.getMapKit());
			}
		});
	}
}
