package pucrs.myflight.modelo;

public class Aeroporto implements Comparable<Aeroporto> {
	private String codigo;
	private String nome;
	private Geo loc;
	private String codigoPais;
	private int rotasRelacionadas;



	public Aeroporto(String codigo, String nome, Geo loc, String codigoPais) {
		this.codigo = codigo;
		this.nome = nome;
		this.loc = loc;
		this.codigoPais = codigoPais;
		this.rotasRelacionadas = 0;
	}

	public int getRotasRelacionadas() {
		return rotasRelacionadas;
	}

	public String getCodigoPais() {
		return codigoPais;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

	public Geo getLocal() {
		return loc;
	}

	public void incrementaRotasRelacionadas(int n){
		rotasRelacionadas = rotasRelacionadas+n;
	}

	@Override
	public int compareTo(Aeroporto o) {
		return nome.compareTo(o.nome);
	}

	@Override
	public String toString() {
		return codigo + " - " + nome + " - " + loc + " - " + codigoPais + " - " + rotasRelacionadas;
	}
}