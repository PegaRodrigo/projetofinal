package pucrs.myflight.modelo;

import java.io.IOException;

public interface ProcessadorArquivos {
	void carregaDados() throws IOException;
}
