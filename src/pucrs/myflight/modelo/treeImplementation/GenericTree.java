package pucrs.myflight.modelo.treeImplementation;

import java.util.*;

/**
 * Created by Rodrigo on 20/06/2017.
 */
public class GenericTree<T> {
    // Classe interna Node
    private class Node {

        public Node father;
        public T element;
        public ArrayList<Node> subtrees;

        public Node(T element) {
            father = null;
            this.element = element;
            subtrees = new ArrayList<>();
        }

        public Node(T element, Node father) {
            this.father = father;
            this.element = element;
            subtrees = new ArrayList<>();
        }

        public void addSubtree(Node n) {
            n.father = this;
            subtrees.add(n);
        }

        public boolean removeSubtree(Node n) {
            n.father = null;
            return subtrees.remove(n);
        }

        public Node getSubtree(int i) {
            if ((i < 0) || (i >= subtrees.size())) {
                throw new IndexOutOfBoundsException();
            }
            return subtrees.get(i);
        }

        public int getSubtreesSize() {
            return subtrees.size();
        }
    }

    // Atributos
    private Node root;
    private int count;

    // Metodos
    public GenericTree() {
        root = null;
        count = 0;
    }

    public T getRoot() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        return root.element;
    }

    public void setRoot(T element) {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        root.element = element;
    }

    public boolean isRoot(T element) {
        if (root != null) {
            if (root.element.equals(element)) {
                return true;
            }
        }
        return false;
    }

    public boolean isEmpty() {
        return (root == null);
    }

    public int size() {
        return count;
    }

    public void clear() {
        root = null;
        count = 0;
    }

    public T getFather(T element) {
        Node n = searchNodeRef(element, root);
        if (n == null || n.father == null) {
            return null;
        } else {
            return n.father.element;
        }
    }

    public boolean contains(T element) {
        Node nAux = searchNodeRef(element, root);
        return (nAux != null);
    }

    public T getChild(T child) {
        Node node = searchNodeRef(child, root);
        return Objects.nonNull(node) ? node.element : null;
    }

    private Node searchNodeRef(T element, Node target) {
        Node res = null;
        if (target != null) {
            if (element.equals(target.element)) {
                res = target;
            } else {
                Node aux = null;
                int i = 0;
                while ((aux == null) && (i < target.getSubtreesSize())) {
                    aux = searchNodeRef(element, target.getSubtree(i));
                    i++;
                }
                res = aux;
            }
        }
        return res;
    }

    public boolean add(T element, T father) {
        Node n = new Node(element);
        Node nAux = null;
        boolean res = false;
        if (father == null) {   // Insere na raiz
            if (root != null) { //Atualiza o pai da raiz
                n.addSubtree(root);
                root.father = n;
            }
            root = n;   //Atualiza a raiz
            res = true;
        } else {        //Insere no meio da Árvore
            nAux = searchNodeRef(father, root);
            if (nAux != null) {
                nAux.addSubtree(n);
                res = true;
            }
        }
        count++;
        return res;
    }


    public ArrayList<T> positionsPre() {
        ArrayList<T> lista = new ArrayList<>();
        positionsPreAux(root, lista);
        return lista;
    }

    private void positionsPreAux(Node n, ArrayList<T> lista) {
        if (n != null) {
            lista.add(n.element);
            for (int i = 0; i < n.getSubtreesSize(); i++) {
                positionsPreAux(n.getSubtree(i), lista);
            }
        }
    }

    public ArrayList<T> positionsWidth() {
        ArrayList<T> lista = new ArrayList<>();

        Queue<Node> fila = new LinkedList<>();
        Node atual;

        if (root != null) {
            fila.offer(root);
            while (!fila.isEmpty()) {
                atual = fila.poll();
                lista.add(atual.element);
                for (int i = 0; i < atual.getSubtreesSize(); i++) {
                    fila.offer(atual.getSubtree(i));
                }
            }
        }
        return lista;
    }
}

