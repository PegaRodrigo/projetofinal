package pucrs.myflight.modelo.gerenciadores;

import pucrs.myflight.modelo.Aeroporto;
import pucrs.myflight.modelo.Geo;
import pucrs.myflight.modelo.ProcessadorArquivos;
import pucrs.myflight.modelo.treeImplementation.GenericTree;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class GerenciadorAeroportos implements Iterable<Aeroporto>, ProcessadorArquivos {

    private Map<String, Aeroporto> aeroportos;

    public GerenciadorAeroportos() {
        aeroportos = new HashMap<String, Aeroporto>();
    }

    public void adicionar(Aeroporto aeroporto) {
        aeroportos.put(aeroporto.getCodigo(), aeroporto);
    }

    public ArrayList<Aeroporto> listarTodos() {
        return new ArrayList<Aeroporto>(aeroportos.values());
    }

    public List<String> listarNomes() {
        return aeroportos.values()
                .parallelStream()
                .sorted(Comparator.comparing(Aeroporto::getNome))
                .map(aeroporto -> aeroporto.getNome())
                .collect(Collectors.toList());
    }

    public List<String> listagemConsulta3() {
        return aeroportos.values()
                .parallelStream()
                .sorted(Comparator.comparing(Aeroporto::getNome))
                .map(aeroporto -> aeroporto.getNome() + " - " + aeroporto.getCodigo())
                .collect(Collectors.toList());
    }

    public void ordenaNome() {
        aeroportos.values().parallelStream().sorted(Comparator.comparing(Aeroporto::getNome));
    }

    public Aeroporto buscarCodigo(String codigo) {
        return aeroportos.get(codigo);
    }

    public Aeroporto buscarGeoIndex(Double latitude, Double longitude) {
        Aeroporto aeroportoFound = null;
        Double distancia = 150.00;
        Geo geo;
        for (Aeroporto aeroporto : aeroportos.values()) {
            geo = new Geo(latitude, longitude);
            Double novaDistancia = geo.distancia(new Geo(aeroporto.getLocal().getLatitude(), aeroporto.getLocal().getLongitude()));
            if (novaDistancia < distancia) {
                distancia = novaDistancia;
                aeroportoFound = aeroporto;
            }
        }

        return aeroportoFound;
    }

    @Override
    public void carregaDados() throws IOException {
        Path path1 = Paths.get("resources/airports.dat");
        try (Scanner sc = new Scanner(Files.newBufferedReader(path1, Charset.forName("utf8")))) {
            sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
            sc.nextLine(); // pula cabeçalho
            String codigo, nome, codigoPais;
            Geo geoLoc;
            Double latitude;
            Double longitude;

            while (sc.hasNextLine()) {
                codigo = sc.next().trim();
                latitude = Double.valueOf(sc.next().trim());
                longitude = Double.valueOf(sc.next().trim());
                geoLoc = new Geo(latitude, longitude);
                nome = sc.next();
                codigoPais = sc.next().trim();

                adicionar(new Aeroporto(codigo, nome, geoLoc, codigoPais));
                sc.nextLine();
            }
        }
    }


    public int rotasAeroportoMaisMovimentado(String codPais) {
    	int maior=0;
    	for(Aeroporto aero: aeroportos.values()){
    		if(codPais.equals("Todos") || aero.getCodigoPais().equals(codPais)){
    			if(aero.getRotasRelacionadas() > maior){
	    			maior = aero.getRotasRelacionadas();
	    		}
    		}
    	}
    	return maior;
    }

    public int rotasAeroportoMaisMovimentado() {
    	int maior=0;
    	for(Aeroporto aero: aeroportos.values()){
    			if(aero.getRotasRelacionadas() > maior){
	    			maior = aero.getRotasRelacionadas();
	    		}
    	}
    	return maior;
    }

    @Override
    public Iterator<Aeroporto> iterator() {
        return new IteratorLista();
    }

    private class IteratorLista implements Iterator<Aeroporto> {
        int index = 0;

        @Override
        public boolean hasNext() {
            return !aeroportos.isEmpty();
        }

        @Override
        public Aeroporto next() {
            Aeroporto aeroporto = ((ArrayList<Aeroporto>) aeroportos.values()).get(index);
            index++;
            return aeroporto;
        }
    }

    @Override
    public String toString() {
        StringBuilder aux = new StringBuilder();
        aeroportos.values()
                .stream()
                .forEach(aeronave -> aux.append(aeronave.toString() + "\n"));

        return aux.toString();
    }
}
