package pucrs.myflight.modelo.gerenciadores;

import pucrs.myflight.modelo.CiaAerea;
import pucrs.myflight.modelo.ProcessadorArquivos;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class GerenciadorCias implements Iterable<CiaAerea>, ProcessadorArquivos {
    private Map<String, CiaAerea> empresas;

    public GerenciadorCias() {
        empresas = new HashMap<String, CiaAerea>();
    }

    public void adicionar(CiaAerea cia) {
        empresas.put(cia.getCodigo(), cia);
    }

    public void gravaSerial() throws IOException {
        Path arq1 = Paths.get("cias.ser");
        try (ObjectOutputStream oarq = new ObjectOutputStream(Files.newOutputStream(arq1))) {
            oarq.writeObject(empresas);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    @SuppressWarnings("unchecked")
    public void carregaSerial() throws IOException {
        Path arq1 = Paths.get("cias.ser");
        try (ObjectInputStream iarq = new ObjectInputStream(Files.newInputStream(arq1))) {
            empresas = (Map<String, CiaAerea>) iarq.readObject();
        } catch (ClassNotFoundException e) {
            System.out.println("Classe CiaAerea não encontrada!");
            System.exit(1);
        }
    }

    @Override
    public void carregaDados() throws IOException {
        Path path1 = Paths.get("resources/airlines.dat");
        try (Scanner sc = new Scanner(Files.newBufferedReader(path1, Charset.forName("utf8")))) {
            sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
            sc.nextLine(); // pula cabeçalho
            String id, nome;
            while (sc.hasNext()) {
                id = sc.next();
                nome = sc.next();

                adicionar(new CiaAerea(id, nome));
            }
        }
    }

    public ArrayList<CiaAerea> listarTodas() {
        return new ArrayList<CiaAerea>(empresas.values());
    }

    public ArrayList<String> listarNomes() {
        ArrayList<String> countries = new ArrayList();

        countries = (ArrayList<String>) empresas.values().parallelStream()
                .map(ciaAerea -> ciaAerea.getNome())
                .sorted()
                .collect(Collectors.toList());
        return countries;
    }

    public CiaAerea buscarCodigo(String codigo) {
        return empresas.get(codigo);
    }

    public CiaAerea buscarNome(String nome) {
        return empresas.values()
                .stream()
                .filter(empresa -> empresa.getNome().equals(nome))
                .findFirst()
                .get();
    }

    @Override
    public Iterator<CiaAerea> iterator() {
        return new IteratorLista();
    }

    private class IteratorLista implements Iterator<CiaAerea> {
        int index = 0;

        @Override
        public boolean hasNext() {
            return !empresas.isEmpty();
        }

        @Override
        public CiaAerea next() {
            CiaAerea ciaAerea = ((ArrayList<CiaAerea>) empresas.values()).get(index);
            index++;
            return ciaAerea;
        }
    }
}
