package pucrs.myflight.modelo.gerenciadores;

import pucrs.myflight.modelo.Aeronave;
import pucrs.myflight.modelo.ProcessadorArquivos;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GerenciadorAeronaves implements ProcessadorArquivos {

	private Map<String,Aeronave> aeronaves;

	public GerenciadorAeronaves() {
		aeronaves = new HashMap<String, Aeronave>();
	}

	public void ordenaCodigo() {
		aeronaves.values().parallelStream().sorted(Comparator.comparing(Aeronave::getCodigo));
	}

	public void ordenaDescricao() {
		aeronaves.values()
		.parallelStream()
		.sorted(Comparator.comparing(Aeronave::getDescricao).reversed());
	}

	public void adicionar(Aeronave av) {
		aeronaves.put(av.getCodigo(),av);
	}

	public ArrayList<Aeronave> listarTodas() {
		return new ArrayList<Aeronave>(aeronaves.values());
	}

	public Aeronave buscarCodigo(String codigo) {
		return aeronaves.get(codigo);
	}

	@Override
	public void carregaDados() throws IOException {
		Path path = Paths.get("resources/equipment.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path, Charset.forName("utf8")))){
			sc.nextLine();
			sc.useDelimiter("[;\n]");

			String codigo, descricao;
			int capacidade;

			while (sc.hasNextLine()) {
				codigo = sc.next();
				descricao = sc.next();
				capacidade = Integer.parseInt(sc.next().trim());

				adicionar(new Aeronave(codigo, descricao, capacidade));
				sc.nextLine();
			}
		} catch (IOException e) {
			System.out.println("Arquivo n�o encontrado.");
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		StringBuilder aux = new StringBuilder();

		aeronaves.values()
		.stream()
		.forEach(aeronave -> aux.append(aeronave.toString()+"\n"));

		return aux.toString();
	}
}
