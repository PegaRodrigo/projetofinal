package pucrs.myflight.modelo.gerenciadores;

import pucrs.myflight.modelo.Aeronave;
import pucrs.myflight.modelo.Aeroporto;
import pucrs.myflight.modelo.CiaAerea;
import pucrs.myflight.modelo.Rota;
import pucrs.myflight.modelo.treeImplementation.GenericTree;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class GerenciadorRotas {

    private Map<String, Rota> rotas;
    private List<Rota> consulta1Rotas;
    private GerenciadorAeronaves gcAeronaves;
    private GerenciadorCias gcCias;
    private GerenciadorAeroportos gcAeroportos;
    private Map<String, List<Rota>> rotasByOrigem;
    private Map<String, List<Rota>> rotasByDestino;

    public GerenciadorRotas(GerenciadorCias gcCias, GerenciadorAeroportos gcAeroportos, GerenciadorAeronaves gcAeronaves) {
    	consulta1Rotas = new ArrayList<Rota>();
        rotas = new HashMap<>();
        this.gcCias = gcCias;
        this.gcAeroportos = gcAeroportos;
        this.gcAeronaves = gcAeronaves;
    }

    public void adicionar(Rota r) {
        String key = r.getOrigem().getCodigo().trim().concat(r.getDestino().getCodigo().trim());
        rotas.put(key, r);
    }

    public int totalRotas() {
        return rotas.size();
    }



    public Map<String, Rota> getRotasMaps() {
        return rotas;
    }

    public void separarPorOrigemEdestino(Rota rota) {
        rotasByOrigem = new HashMap<>();
        rotasByDestino = new HashMap<>();

        Aeroporto origem = rota.getOrigem();
        Aeroporto destino = rota.getDestino();

        distribuirOrigemDestino(origem, destino, rota);

    }

    private void distribuirOrigemDestino(Aeroporto origem, Aeroporto destino, Rota rota) {
        if (rotasByOrigem.containsKey(origem.getCodigo())) {
            List<Rota> rotasAlteradas = rotasByOrigem.get(origem.getCodigo());
            rotasAlteradas.add(rota);
            rotasByOrigem.replace(origem.getCodigo(), new ArrayList<>(rotasAlteradas));
        } else if (origem.getCodigo().equals(origem.getCodigo())) {
            List<Rota> rotas = new ArrayList<>();
            rotas.add(rota);
            rotasByOrigem.put(origem.getCodigo(), rotas);
        }

        if (rotasByDestino.containsKey(destino.getCodigo())) {
            List<Rota> rotasAlteradas = rotasByDestino.get(destino.getCodigo());
            rotasAlteradas.add(rota);
            rotasByDestino.replace(destino.getCodigo(), new ArrayList<>(rotasAlteradas));
        } else if (destino.getNome().equals(destino.getCodigo())) {
            List<Rota> rotas = new ArrayList<>();
            rotas.add(rota);
            rotasByDestino.put(destino.getCodigo(), rotas);
        }
    }

    public Map<String, List<Rota>> getRotasByOrigem() {
        return rotasByOrigem;
    }

    public Map<String, List<Rota>> getRotasByDestino() {
        return rotasByDestino;
    }

    public void carregaDados() throws IOException {
        Path path = Paths.get("resources/routes.dat");
        try (Scanner sc = new Scanner(Files.newBufferedReader(path, Charset.forName("utf8")))) {
            sc.nextLine();
            sc.useDelimiter("[;\n]");

            String aeronaveCodigo, aeroportoOrigemCodigo, aeroportoDestinoCodigo, ciaAereaCodigo;

            while (sc.hasNext()) {
                ciaAereaCodigo = sc.next().trim();
                aeroportoOrigemCodigo = sc.next().trim();
                aeroportoDestinoCodigo = sc.next().trim();
                sc.next();
                sc.next();
                aeronaveCodigo = sc.next().split(" ")[0].trim();

                Aeronave aeronave = gcAeronaves.buscarCodigo(aeronaveCodigo);
                Aeroporto aeroportoOrigem = gcAeroportos.buscarCodigo(aeroportoOrigemCodigo);
                Aeroporto aeroportoDestino = gcAeroportos.buscarCodigo(aeroportoDestinoCodigo);
                CiaAerea cia = gcCias.buscarCodigo(ciaAereaCodigo);
                //Incrementa 1 � quantidade de rotas relacionadas � cada aeroporto:
                aeroportoOrigem.incrementaRotasRelacionadas(1);
                aeroportoDestino.incrementaRotasRelacionadas(1);
                //---
                Rota rota = new Rota(cia, aeroportoOrigem, aeroportoDestino, aeronave);
              
                consulta1Rotas.add(rota);
                adicionar(rota);
                separarPorOrigemEdestino(rota);
            }
        } catch (IOException e) {
            System.out.println("Arquivo n�o encontrado.");
            e.printStackTrace();
        }

    }
    
    public List<Rota> getRotasList(){
    	return new ArrayList<>(consulta1Rotas);
    }

}
