package pucrs.myflight.modelo.gerenciadores;

import pucrs.myflight.modelo.Pais;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class GerenciadorPaises {

    private Map<String, Pais> paises;

    public GerenciadorPaises() {
        paises = new HashMap<String, Pais>();
    }

    public void ordenaCodigo() {
        paises.values().parallelStream().sorted(Comparator.comparing(Pais::getCodigo));
    }

    public void ordenaNome() {
        paises.values().parallelStream().sorted(Comparator.comparing(Pais::getNome));
    }

    public void adicionar(Pais av) {
        paises.put(av.getCodigo(), av);
    }

    public ArrayList<Pais> listarTodas() {
        return new ArrayList<Pais>(paises.values());
    }

    public ArrayList<String> listarNomes() {
        ArrayList<String> countries = new ArrayList();

        countries = (ArrayList<String>) paises.values().parallelStream()
                .map(pais -> pais.getNome()+" - "+pais.getCodigo())
                .sorted()
                .collect(Collectors.toList());
        countries.add(0,  "Todos");
        return countries;
    }

    public Pais buscarCodigo(String codigo) {
        return paises.get(codigo);
    }

    public void carregaDados() throws IOException {
        Path path = Paths.get("resources/countries.dat");
        try (Scanner sc = new Scanner(Files.newBufferedReader(path, Charset.forName("utf8")))) {
            sc.nextLine();
            sc.useDelimiter("[;\n]");
            String codigo, nome;

            while (sc.hasNextLine()) {
                codigo = sc.next();
                nome = sc.next();

                adicionar(new Pais(codigo, nome));
                sc.nextLine();
            }
        } catch (IOException e) {
            System.out.println("Arquivo n�o encontrado.");
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        StringBuilder aux = new StringBuilder();
        paises.values()
                .stream()
                .forEach(pais -> aux.append(pais.toString() + "\n"));
        return aux.toString();
    }
}
