package pucrs.myflight.modelo;

import java.io.Serializable;

public class CiaAerea implements Serializable {

	private static final long serialVersionUID = 6380611754334520520L;
	private String codigo;
	private String nome;

	public CiaAerea(String codigo, String nome) {
		this.codigo = codigo;
		this.nome = nome;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}	

	@Override
	public String toString() {
		return codigo.concat(nome);
	}
}
