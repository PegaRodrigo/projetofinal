package pucrs.myflight.modelo;

public class Pais implements Comparable<Pais> {
    private String codigo;
    private String nome;

    public Pais(String codigo, String nome) {
        if (codigo == null)
            throw new IllegalArgumentException("Código não pode ser nulo!");
        this.codigo = codigo;
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return codigo + " - " + nome;
    }

    @Override
    public int compareTo(Pais o) {
        return codigo.compareTo(o.codigo);
    }
}
